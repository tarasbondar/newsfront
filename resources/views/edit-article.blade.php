@extends('layouts.admin')

@section('content')

    <div class="container">

        <h2>{{ucfirst($action)}} article</h2>

        <div class="row justify-content-center">
            <div class="col-md-10">

                <form method="POST" action="/admin/article/save" enctype = 'multipart/form-data'>
                    @csrf
                    <div class="form-group row" hidden>
                        <input id="id" type="text" class="form-control" name="id" value="{{ @$article['id'] }}" readonly>
                    </div>
                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>
                        <div class="col-md-6"> <input id="title" type="text" class="form-control " name="title" value="{{ @$article['title'] }}" required> </div>
                    </div>
                    <div class="form-group row">
                        <label for="teaser" class="col-md-4 col-form-label text-md-right">Teaser</label>
                        <div class="col-md-6"> <textarea id="teaser" type="text" class="form-control " name="teaser" rows="3"> {{ @$article['teaser'] }} </textarea></div>
                    </div>
                    <div class="form-group row">
                        <label for="text" class="col-md-4 col-form-label text-md-right">Text</label>
                        <div class="col-md-6"> <textarea id="text" type="text" class="form-control " name="text" rows="6" required> {{ @$article['text'] }} </textarea> </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Current Image</label>
                        <div class="col-md-6"> <div class="form-control">{{ $article['image'] ?? 'none' }} </div></div>
                    </div>
                    <div class="form-group row">
                        <label for="image" class="col-md-4 col-form-label text-md-right">Choose Image</label>
                        <div class="col-md-6"> <input id="image" type="file" name="image"></div>
                    </div>
                    <div class="form-group row">
                        <label for="topics[]" class="col-md-4 col-form-label text-md-right">Topics</label>
                        <div class="col-md-6">
                            @foreach($topics as $t)
                                <input type="checkbox" name="topics[]" value="{{$t['id']}}" @if (in_array($t['id'], $a2t)) {{ 'checked' }} @endif> {{$t['name']}} <br/>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary"> Save </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

