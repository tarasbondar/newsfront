@extends('layouts.admin')

@section('content')

    <div class="container">

        <h2>TOPICS</h2>

        <div class="row">
            <div class="col-md-12 text-right mt-3 mb-3">
                <a href="/admin/topic/add" class="btn btn-primary" type="button">Add topic</a>
            </div>
        </div>

        @if (!empty($topics))
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($topics as $t)
                    <tr>
                        <th scope="row">{{ $t['id'] }}</th>
                        <td>{{ $t['name'] }}</td>
                        <td>{{ $t['description'] }}</td>
                        <td>
                            <a href="#" class="edit-topic edit-{{ $t['id'] }}">Edit</a>
                            <a href="#" class="remove-topic remove-{{ $t['id'] }}">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif


        <script>
            (function(){
                $(document).on('click', '.edit-topic', function(){
                    let button = $(this);

                    let classes = button.attr('class').split(" ");
                    let topic_id = 0;
                    $.each(classes, function (k, v) {
                        if (v.search('edit-') >= 0) {
                            topic_id = v.split("-")[1];
                        }
                    });

                    window.location.href = '/admin/topic/edit/' + topic_id;

                });

                $(document).on('click', '.remove-topic', function(){
                    let button = $(this);

                    let classes = button.attr('class').split(" ");
                    let topic_id = 0;
                    $.each(classes, function (k, v) {
                        if (v.search('remove-') >= 0) {
                            topic_id = v.split("-")[1];
                        }
                    });

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'/admin/topic/remove',
                        data: {'topic_id': topic_id},
                        method: 'POST',
                        success: function(response) {
                            window.location.href = '/admin/topics';
                        }
                    });
                });

            })(jQuery)
        </script>
    </div>
@endsection

