@extends('layouts.admin')

@section('content')

    <div class="container">

        <h2>Articles</h2>

        <div class="row">
            <div class="col-md-12 text-right mt-3 mb-3">
                <a href="/admin/article/add" class="btn btn-primary" type="button">Add article</a>
            </div>
        </div>

        @if (!empty($articles))
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Teaser</th>
                    <th scope="col">Author</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($articles as $a)
                    <tr>
                        <th scope="row">{{ $a['id'] }}</th>
                        <td>{{ $a['title'] }}</td>
                        <td>{{ $a['teaser'] }}</td>
                        <td>{{ $a['author_id'] }}</td>
                        <td>
                            <a href="#" class="edit-article edit-{{ $a['id'] }}">Edit</a>
                            <a href="#" class="remove-article remove-{{ $a['id'] }}">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif


        <script>
            (function(){
                $(document).on('click', '.edit-article', function(){
                    let button = $(this);
                    let classes = button.attr('class').split(" ");
                    let article_id = 0;
                    $.each(classes, function (k, v) {
                        if (v.search('edit-') >= 0) {
                            article_id = v.split("-")[1];
                        }
                    });
                    window.location.href = '/admin/article/edit/' + article_id;
                });

                $(document).on('click', '.remove-article', function(){
                    let button = $(this);
                    let classes = button.attr('class').split(" ");
                    let article_id = 0;
                    $.each(classes, function (k, v) {
                        if (v.search('remove-') >= 0) {
                            article_id = v.split("-")[1];
                        }
                    });

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:'/admin/topic/remove',
                        data: {'article_id': article_id},
                        method: 'POST',
                        success: function(response) {
                            window.location.href = '/admin/articles';
                        }
                    });
                });

            })(jQuery)
        </script>
    </div>
@endsection

