@extends('layouts.admin')

@section('content')

<div class="container">

    <h2>{{ucfirst($action)}} user</h2>

    <div class="row justify-content-center">
        <div class="col-md-10">

            <form method="POST" action="/admin/user/save">
                @csrf
                <div class="form-group row" hidden>
                    <input id="id" type="text" class="form-control" name="id" value="{{ @$user['id'] }}" readonly>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                    <div class="col-md-6"> <input id="name" type="text" class="form-control " name="name" value="{{ @$user['name'] }}" required> </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                    <div class="col-md-6"> <input id="email" type="email" class="form-control " name="email" value="{{ @$user['email'] }}" required> </div>
                </div>

                <div class="form-group row">
                    <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>
                    <div class="col-md-6">
                        <select id="status-select" class="form-control col-md-6" name="status">
                            <option value="0">User</option>
                            <option value="1" {{ ($user['status'] == 1 ? 'selected' : '') }}>Admin</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary"> Save </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
