<div class="sidebar-sticky admin-sidebar">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link active" href="/admin">
                Dashboard <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/users">Users</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/topics">Topics</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/articles">Articles</a>
        </li>
    </ul>
</div>

