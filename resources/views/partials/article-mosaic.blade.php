<div class="mosaic-container">
    <div class="mosaic-title"><p>{{ $article->title }}</p></div>
    <div class="mosaic-date"><p>{{ $article->updated_at }}</p></div>
    <div class="mosaic-story"> {{ $article->text }} </div>
    <div class="mosaic-topics"> {{ count($article->topics) }} </div>
</div>
