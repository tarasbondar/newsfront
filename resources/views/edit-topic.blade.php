@extends('layouts.admin')

@section('content')

    <div class="container">

        <h2>{{ucfirst($action)}} topic</h2>

        <div class="row justify-content-center">
            <div class="col-md-12">

                <form method="POST" action="/admin/topic/save">
                    @csrf
                    <div class="form-group row" hidden>
                        <input id="id" type="text" class="form-control" name="id" value="{{ $action == 'edit' ? $topic['id'] : '0' }}" readonly>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-3 col-form-label text-md-right">Name</label>
                        <div class="col-md-6"> <input id="name" type="text" class="form-control " name="name" value="{{ @$topic['name'] }}" required> </div>
                    </div>

                    <div class="form-group row">
                        <label for="description" class="col-md-3 col-form-label text-md-right">Description</label>
                        <div class="col-md-6"> <input id="description" type="text" class="form-control " name="description" value="{{ @$topic['description'] }}" required> </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-3">
                            <button type="submit" class="btn btn-primary"> Save </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

