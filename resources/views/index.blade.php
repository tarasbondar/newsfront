@extends('layouts.front')

@section('content')
    @if (!empty($articles))
        @foreach($articles as $article)
            @include('partials.article-mosaic')
        @endforeach
    @endif
@endsection
