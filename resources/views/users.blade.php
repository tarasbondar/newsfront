@extends('layouts.admin')

@section('content')

    <div class="container">

    <h2>USERS</h2>

    @if (!empty($users))
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Status</th>
                    <th scope="col">Registered</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $u)
                <tr>
                    <th scope="row">{{ $u['id'] }}</th>
                    <td>{{ $u['name'] }}</td>
                    <td>{{ $u['email'] }}</td>
                    <td>{{ ($u['status'] == 1 ? 'admin' : 'user') }}</td>
                    <td>{{ explode(' ', $u['created_at'])[0] }}</td>
                    <td>
                        <a href="#" class="edit-user edit-{{ $u['id'] }}">Edit</a>
                        <a href="#" class="remove-user remove-{{ $u['id'] }}">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div>
            <form action="/admin/user/export" method="POST" name="xls-download" enctype="multipart/form-data">
                @csrf
                <button type="submit" class="btn btn-primary xls-download">Download as xls</button>
            </form>
        </div>
        @endif
    </div>


    <script>
        (function(){
            $(document).on('click', '.edit-user', function(){
                let button = $(this);

                let classes = button.attr('class').split(" ");
                let user_id = 0;
                $.each(classes, function (k, v) {
                    if (v.search('edit-') >= 0) {
                        user_id = v.split("-")[1];
                    }
                });

                window.location.href = '/admin/user/edit/' + user_id;

            });

            $(document).on('click', '.remove-user', function(){
                let button = $(this);

                let classes = button.attr('class').split(" ");
                let user_id = 0;
                $.each(classes, function (k, v) {
                    if (v.search('remove-') >= 0) {
                        user_id = v.split("-")[1];
                    }
                });

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url:'/admin/user/remove',
                    data: {'user_id': user_id},
                    method: 'POST',
                    success: function(response) {
                        window.location.href = '/admin/users';
                    }
                });
            });

        })(jQuery)
    </script>

@endsection
