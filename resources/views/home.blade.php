@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">


                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h6 class="border-bottom border-gray pb-2 mb-0">Available subscriptions</h6>
                        @foreach($topics as $topic)
                            <div class="media text-muted pt-3">
                                <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                    <div class="d-flex justify-content-between align-items-center w-100">
                                        <strong class="text-gray-dark">{{ $topic['name'] }}</strong>
                                        @if (!empty($subs) && in_array($topic['id'], $subs))
                                            <button type="button" class="btn btn-sm btn-danger unsubscribe topic-{{$topic['id']}}">Unsubscribe</button>
                                        @else
                                            <button type="button" class="btn btn-sm btn-primary subscribe topic-{{$topic['id']}}">Subscribe</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>

    (function(){
        $(document).on('click', '.subscribe', function(){
            let button = $(this);

            let classes = button.attr('class').split(" ");
            let topic_id = 0;
            $.each(classes, function (k, v) {
                if (v.search('topic-') >= 0) {
                    topic_id = v.split("-")[1];
                }
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'/subscribe',
                data: {'topic_id': topic_id},
                method: 'POST',
                success: function(response) {
                    if (response === '') {
                        button.removeClass('subscribe btn-primary').addClass('unsubscribe btn-danger').html('Unsubscribe');
                    } else {
                        window.alert('Something wrong has happened!');
                    }
                }
            });

        });

        $(document).on('click', '.unsubscribe', function(){
            let button = $(this);

            let classes = button.attr('class').split(" ");
            let topic_id = 0;
            $.each(classes, function (k, v) {
                if (v.search('topic-') >= 0) {
                    topic_id = v.split("-")[1];
                }
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'/unsubscribe',
                data: {'topic_id': topic_id},
                method: 'POST',
                success: function(response) {
                    if (response === '') {
                        button.removeClass('unsubscribe btn-danger').addClass('subscribe btn-primary').html('Subscribe');
                    } else {
                        window.alert('Something wrong has happened!');
                    }
                }
            });

        })

    })(jQuery)

</script>

@endsection


