<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@front.loc',
            'password' => '$2y$10$JfwCFEFIp8UjkkF6iGIecu4o7lhyBHu2/1kl3td2djxKGguO0S5qe', //12345678
            'status' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('topics')->insert([
            'name' => 'New topic',
        ]);
    }
}
