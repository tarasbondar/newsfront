<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class Article2Topic extends Model {

    protected $table = 'topics_articles';

    public $timestamps = false;

    public $fillable = ['topic_id', 'article_id'];

}
