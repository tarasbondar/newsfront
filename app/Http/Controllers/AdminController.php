<?php

namespace App\Http\Controllers;

use App\Article;
use App\Article2Topic;
use App\Topic;
use App\User;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            if (Auth::user()->status < 1) {
                return redirect('/');
            }
            return $next($request);
        });
    }

    public function index() {
        return view('dashboard');
    }

    public function users() {
        $users = User::all()->toArray();
        return view('users', ['users' => $users]);
    }

    public function userEdit($id) {
        $user = User::find($id);
        return view('edit-user', ['user' => $user]);
    }

    public function userSave(Request $request) {
        $user = User::find($request['id']);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->status = $request['status'];
        $user->save();
        return redirect()->action('AdminController@users');
    }

    public function userRemove(Request $request) {
        User::destroy($request->get('user_id'));
        return '';
    }

    public function userExport() {
        return Excel::download(new UsersExport, 'users.xlsx');
    }


    public function topics() {
        $topics = Topic::all()->toArray();
        return view('topics', ['topics' => $topics]);
    }

    public function topicAdd() {
        return view('edit-topic', ['action' => 'add']);
    }

    public function topicEdit($id) {
        $topic = Topic::find($id);
        return view('edit-topic', ['action' => 'edit', 'topic' => $topic]);
    }

    public function topicSave(Request $request) {
        if ($request['id'] == 0) {
            $topic = new Topic();
        } else {
            $topic = Topic::find($request['id']);
        }

        $topic->name = $request['name'];
        $topic->description = $request['description'];
        $topic->save();
        return redirect()->action('AdminController@topics');
    }

    public function topicRemove(Request $request) {
        Topic::destroy($request->get('topic_id'));
        return '';
    }



    public function articles() {
        $articles = Article::all()->toArray();
        return view('articles', ['articles' => $articles]);
    }

    public function articleAdd() {
        $topics = Topic::all()->toArray();
        return view('edit-article', ['action' => 'add', 'topics' => $topics, 'a2t' => []]);
    }

    public function articleEdit($id) {
        $article = Article::find($id);
        $topics = Topic::all()->toArray();
        $a2t = Article2Topic::where('article_id', $id)->pluck('topic_id')->toArray();
        return view('edit-article', ['action' => 'add', 'topics' => $topics, 'article' => $article, 'a2t' => $a2t]);
    }

    public function articleSave(Request $request) {
        $new = true;
        if (empty($request['id'])) {
            $article = new Article();
            $article->author_id = Auth::user()->id;
        } else {
            $new = false;
            $article = Article::find($request['id']);
        }
        $article->title = $request['title'];
        $article->teaser = $request['teaser'];
        $article->text = $request['text'];

        if ($request->has('image')) {
            if (isset($article->image)) {
                File::delete(public_path('uploads') . '/' . $article->image);
            }
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $request->image->move(public_path('uploads'), $filename);
            $article->image = $filename;
        }

        $article->save();

        $a2t = [];
        if (!$new) {
            $a2t = Article2Topic::where('article_id', $article->id)->pluck('topic_id')->toArray();
        }

        if (empty($request['topics'])) {
            $to_delete = Article2Topic::where('article_id', $article->id)->get();
            if (!empty($to_delete)) {
                foreach ($to_delete as $item) {
                    $item->delete();
                }
            }
        } else {
            if (empty($a2t)) {
                foreach ($request['topics'] as $topic_id) {
                    Article2Topic::create(['topic_id' => $topic_id, 'article_id' => $article->id]);
                }
            } else {
                $to_delete = array_diff($a2t, $request['topics']);
                if (!empty($to_delete)) {
                    foreach ($to_delete as $topic_id) {
                        Article2Topic::where('article_id', $article->id)->where('topic_id', $topic_id)->delete();
                    }
                }

                $to_add = array_diff($request['topics'], $a2t);
                if (!empty($to_add)) {
                    foreach ($to_add as $topic_id) {
                        Article2Topic::create(['topic_id' => $topic_id, 'article_id' => $article->id]);
                    }
                }
            }
        }

        //announce w/ observer
        return redirect()->action('AdminController@articles');

    }

    public function articleRemove(Request $request) {
        $article = Article::find($request['article_id']);
        if (isset($article->image)) {
            File::delete(public_path('uploads') . '/' . $article->image);
        }
        $article->delete();
        return '';
    }


}
