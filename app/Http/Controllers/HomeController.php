<?php

namespace App\Http\Controllers;

use App\Article;
use App\Subscription;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $articles = Article::find(true)->with('topics')->orderBy('updated_at', 'DESC')->take(3)->get();
        return view('index', ['articles' => $articles]);
    }

    public function home() {
        $topics = Topic::all()->toArray();
        $users_subs = Subscription::where('user_id', '=', Auth::id())->pluck('topic_id')->toArray();
        return view('home', ['topics' => $topics, 'subs' => $users_subs]);
    }

    public function subscribe(Request $request) {
        $topic_id = $request->get('topic_id');
        if ($topic_id > 0) {
            $sub = new Subscription();
            $sub->topic_id = $topic_id;
            $sub->user_id = Auth::id();
            if ($sub->save()) {
                return '';
            }
        }
        return 'Something is wrong';
    }

    public function unsubscribe(Request $request) {
        $topic_id = $request->get('topic_id');
        if ($topic_id > 0) {
            $sub = Subscription::where(['topic_id' => $topic_id, 'user_id' => Auth::id()]);
            if (!empty($sub)) {
                DB::table('subscriptions')->where('user_id', '=', Auth::id())->where('topic_id', '=', $topic_id)->delete();
                return '';
            }
        }
        return 'Something is wrong';
    }
}
