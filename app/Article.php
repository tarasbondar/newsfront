<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

protected $table = 'articles';

protected $fillable = ['title', 'teaser', 'text', 'image', 'status'];

public function topics() {
    return $this->belongsToMany('App\Topic', 'topics_articles');
}

}
