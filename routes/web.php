<?php

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@home')/*->name('home')*/;
Route::post('/subscribe', 'HomeController@subscribe');
Route::post('/unsubscribe', 'HomeController@unsubscribe');

Route::get('/admin', 'AdminController@index');

//users
Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/user/edit/{id}', 'AdminController@userEdit');
Route::post('/admin/user/save', 'AdminController@userSave');
Route::post('/admin/user/remove', 'AdminController@userRemove');
Route::post('/admin/user/export', 'AdminController@userExport');

//topics
Route::get('/admin/topics', 'AdminController@topics');
Route::get('/admin/topic/add', 'AdminController@topicAdd');
Route::get('/admin/topic/edit/{id}', 'AdminController@topicEdit');
Route::post('/admin/topic/save', 'AdminController@topicSave');
Route::post('/admin/topic/remove', 'AdminController@topicRemove');

//articles
Route::get('/admin/articles', 'AdminController@articles');
Route::get('/admin/article/add', 'AdminController@articleAdd');
Route::get('/admin/article/edit/{id}', 'AdminController@articleEdit');
Route::post('/admin/article/save', 'AdminController@articleSave');
Route::post('/admin/article/remove', 'AdminController@articleRemove');

